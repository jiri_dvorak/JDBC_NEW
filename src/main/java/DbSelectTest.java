import lombok.AllArgsConstructor;

import java.sql.*;

@AllArgsConstructor
public class DbSelectTest {

    private Connection connection;

    public void selectSimple() throws Exception {

        System.out.println("SIMPLE SELECT ****************************************************");
        // získání výrazu z connection
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        // spusteni dotazu vyrazem ziskani sady vysledku
        ResultSet resultset = statement.executeQuery("SELECT * FROM testtb2");

        // skok na prvni radek sady vysledku (neni nutne pouzit)
        //resultset.first();
        //System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        //
        // prechod na dalsi radek sady vysledku
        while(resultset.next()){
            System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        }
        System.out.println("************************************************************************");
    }

    public void selectCount() throws Exception {
        System.out.println("SELECT COUNT ****************************************************");
        // získání výrazu z connection
        //Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        Statement statement = connection.createStatement();
        // spusteni dotazu vyrazem ziskani sady vysledku
        ResultSet resultset = statement.executeQuery("SELECT COUNT(*) FROM testtb2");
        if(resultset.next()){
            System.out.println("Pocet radku je = (" + resultset.getInt(1) + ")");
        }
        System.out.println("************************************************************************");
    }

    public void selectSimpleWhere() throws Exception {

        System.out.println("SIMPLE WHERE SELECT ****************************************************");
        // získání výrazu z connection
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        // spusteni dotazu vyrazem ziskani sady vysledku
        ResultSet resultset = statement.executeQuery("SELECT * FROM testtb2 WHERE id>6");
        // prechod na dalsi radek sady vysledku
        while(resultset.next()){
            System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        }
        System.out.println("************************************************************************");
    }
    public void selectSimpleCiselnik() throws Exception {
        System.out.println("SIMPLE WHERE SELECT ****************************************************");
        // získání výrazu z connection
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        // spusteni dotazu vyrazem ziskani sady vysledku
        ResultSet resultset = statement.executeQuery(
                "SELECT tb.name, ci.ciselnikname " +
                    "FROM mydb.testtb2 tb, mydb.ciselnik ci " +
                    "WHERE tb.ciselnik_id=ci.id");
        // prechod na dalsi radek sady vysledku
        while (resultset.next()) {
            System.out.println(resultset.getString(1) + " - " + resultset.getString(2) + "");
        }
        System.out.println("************************************************************************");
    }

    public void selectWhereParamWrong(Integer id) throws Exception {
        System.out.println("SIMPLE WHERE PARAM WRONG ****************************************************");
        // získání výrazu z connection
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        // spusteni dotazu vyrazem ziskani sady vysledku
        ResultSet resultset = statement.executeQuery("SELECT * FROM testtb2 WHERE id>" + id.toString());
        // prechod na dalsi radek sady vysledku
        while(resultset.next()){
            System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        }
        System.out.println("************************************************************************");
    }

    public void selectWhereParamBest(Integer id1, Integer id2) throws Exception {

        System.out.println("PREPARED WHERE PARAM OK ****************************************************");
        // získání výrazu z connection
        PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM testtb2 WHERE id>? AND id<?");
        prepareStatement.setInt(1, id1);
        prepareStatement.setInt(2, id2);

        ResultSet resultset = prepareStatement.executeQuery();
        // prechod na dalsi radek sady vysledku
        while(resultset.next()){
            System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        }
        System.out.println("************************************************************************");
    }

    public void selectSelectMetadata() throws Exception {

        System.out.println("metadata SELECT ****************************************************");
        Statement statement = connection.createStatement();
        ResultSet resultset = statement.executeQuery("SELECT * FROM testtb2");
        ResultSetMetaData meta = resultset.getMetaData();
        for(int i = 1; i<=meta.getColumnCount(); i++){

            Integer intType = meta.getColumnType(i);

            System.out.print(meta.getColumnName(i) + " ");
            System.out.print(" [" + intType + "] ");
            System.out.print(meta.getColumnTypeName(i));

            if(intType==4){
                System.out.println(" [tento sloupec je INT] ");
            }else{
                System.out.println(" [tento sloupec neni INT] ");
            }
        }

        System.out.println("************************************************************************");
    }


}

