import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

public class DbProcedureTest {

    public static void procTest(Connection connection) throws Exception{

        // jen ukazka moznosti volani db procedur

        CallableStatement calculateStatisticsStatement = connection.prepareCall("{call updatetesttable(?)}");
        calculateStatisticsStatement.setInt(1, 101);
        ResultSet rs = calculateStatisticsStatement.executeQuery();
        while(rs.next()){
            System.out.println(rs.getString(2) + " - (" + rs.getInt(1) + ")");
        }
    }
}
