import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class DbDelete {

    private Connection connection;
    private PreparedStatement prepareStatement;

    public DbDelete(Connection aConnection){
        connection = aConnection;
        try {
            prepareStatement = connection.prepareStatement("DELETE from mydb.testtb2 WHERE id=?");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void doDelete(Integer id) throws SQLException {
        prepareStatement.setInt(1, id);
        Integer count = prepareStatement.executeUpdate();
    }

}
