public class DbTransaction {

    public static void testTransaction() throws Exception {

        // vypnutí autocommitu = jsme v tansakčním rezimu
        // zahájí transakci
        DbConnection.getConnection().setAutoCommit(false);

        DbInsertTrans ins = new DbInsertTrans(DbConnection.getConnection());
        // vkladani radku 11 - 14
        for(Integer i = 11; i<15; i++){
            ins.doInsert(i, "Name-" + i.toString());
        }
        // ukonci trasakci a zahodi vsechny zmeny v db
        // soucasne zahájí novou transakci
        DbConnection.getConnection().rollback();

        // vkladani radku 18 - 25
        for(Integer i = 18; i<26; i++){
            ins.doInsert(i, "Name-" + i.toString());
        }
        // ukonci trasakci potvrdi zmeny  v db
        // soucasne zahájí novou transakci
        DbConnection.getConnection().commit();
    }

    public static void trans() throws Exception {

        DbConnection.getConnection().setAutoCommit(false);
        // ------------------------------------------------
        try{
            // manipulaci s daty db


            DbConnection.getConnection().commit();
        }catch (Exception ex1){
            DbConnection.getConnection().rollback();
        }
        // ------------------------------------------------
        try{
            // manipulaci s daty db


            DbConnection.getConnection().commit();
        }catch (Exception ex2){
            DbConnection.getConnection().rollback();
        }

    }

}
