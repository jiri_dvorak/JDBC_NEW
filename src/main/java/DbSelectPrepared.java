import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbSelectPrepared {

    private Connection connection;
    private PreparedStatement prepareStatement;

    public DbSelectPrepared(Connection aConnection){
        connection = aConnection;
        try {
            prepareStatement = connection.prepareStatement("SELECT * FROM testtb2 WHERE id>? AND id<?");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void selectWhereParams(Integer id1, Integer id2) throws Exception {

        System.out.println("PREPARED SELECT WHERE PARAM ****************************************************");

        prepareStatement.setInt(1, id1);
        prepareStatement.setInt(2, id2);
        ResultSet resultset = prepareStatement.executeQuery();
        while(resultset.next()){
            System.out.println(resultset.getString(2) + " - (" + resultset.getInt(1) + ")");
        }

        System.out.println("************************************************************************");
    }


}
