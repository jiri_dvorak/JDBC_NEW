import org.apache.commons.configuration.ConfigurationException;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {

    private static Connection connection = null;

    public static Connection getConnection() throws Exception {
        if(connection==null){
            connection = DriverManager.getConnection(
                    CfgClass.getConfig().getString("db.connectstring"),
                    CfgClass.getConfig().getString("db.user"),
                    CfgClass.getConfig().getString("db.password"));
        }
        return connection;
    }

    // jen pro mysql nikoli pro jine databaze -------------------------------------
    //MysqlDataSource dataSource = new MysqlDataSource();
    //dataSource.setUrl("jdbc:mysql://localhost:3306/mydb");
    //dataSource.setUser("root");
    //dataSource.setPassword("root");
    //Connection connection = dataSource.getConnection();
    // --------------------------------------------


}
