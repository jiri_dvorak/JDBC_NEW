import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class DbInsert {

    private Connection connection;
    private PreparedStatement prepareStatement;

    public DbInsert(Connection aConnection){
        connection = aConnection;
        try {
            prepareStatement = connection.prepareStatement("INSERT INTO mydb.testtb2 (id,name,cena,ciselnik_id,skupina) VALUES (?,?,?,?,?)");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void doInsert(Integer id, String name, Integer cena, Integer ciselnikId, Integer skupina) throws SQLException {
        prepareStatement.setInt(1, id);
        prepareStatement.setString(2, name);
        prepareStatement.setInt(3, cena);
        prepareStatement.setInt(4, ciselnikId);
        prepareStatement.setInt(5, skupina);
        Integer count = prepareStatement.executeUpdate();
    }

}
