import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class DbInsertTrans {

    private Connection connection;
    private PreparedStatement prepareStatement;

    public DbInsertTrans(Connection aConnection){
        connection = aConnection;
        try {
            prepareStatement = connection.prepareStatement("INSERT INTO mydb.testtablex (id,name) VALUES (?,?)");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void doInsert(Integer id, String name) throws SQLException {
        prepareStatement.setInt(1, id);
        prepareStatement.setString(2, name);
        Integer count = prepareStatement.executeUpdate();
    }

}
