import lombok.AllArgsConstructor;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@AllArgsConstructor
public class DbTable {

    private Connection connection;

    public void createTable() throws Exception {
        Statement statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS testtableX " +
                "( id INT NOT NULL, name VARCHAR(45) NULL, PRIMARY KEY (id))");
    }

    public void dropTable() throws Exception {
        Statement statement = connection.createStatement();
        statement.executeUpdate("DROP TABLE IF EXISTS testtableX");
    }

}
