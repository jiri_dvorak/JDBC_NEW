import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class DbUpdate {

    private Connection connection;
    private PreparedStatement prepareStatement;

    public DbUpdate(Connection aConnection){
        connection = aConnection;
        try {
            prepareStatement = connection.prepareStatement("UPDATE mydb.testtb2 SET name=?, cena=?, ciselnik_id=?, skupina=? WHERE id=?");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void doUpdate(Integer id, String name, Integer cena, Integer ciselnikId, Integer skupina) throws SQLException {
        prepareStatement.setString(1, name);
        prepareStatement.setInt(2, cena);
        prepareStatement.setInt(3, ciselnikId);
        prepareStatement.setInt(4, skupina);
        prepareStatement.setInt(5, id);
        Integer count = prepareStatement.executeUpdate();
    }

}
