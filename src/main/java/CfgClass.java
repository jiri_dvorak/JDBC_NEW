import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class CfgClass {

    private static PropertiesConfiguration config = null;

    public static PropertiesConfiguration getConfig() throws ConfigurationException {
        if(config==null){
            config = new PropertiesConfiguration();
            config.load("application.properties");
        }
        return config;
    }

}
